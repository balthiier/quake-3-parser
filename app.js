//loading libs
const express = require('express');
const app = express();
let port = process.env.PORT || 3000;
let routes = require('./quake-parser');

app.set('view engine', 'pug');
app.set('views', __dirname);
app.use(express.static(__dirname));
//route quake index page
app.use('/', routes);

//run server
app.listen(port, function() {
    console.log('Run Parser: time to Quake 3');
});