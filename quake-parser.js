//loading libs
const express = require('express');
const router = express.Router();
const lineReader = require('readline');
const fs = require('fs');
const rd = lineReader.createInterface({
    input: fs.createReadStream('games.log'),
    output: process.stdout,
    terminal: false
});

//game data structure
let game = {
    total_kills: 0,
    players: [
        {name:  'Oootsimo',       kills: 0},
        {name:  'Zeh',            kills: 0},
        {name:  'Isgalamido',     kills: 0},
        {name:  'Assasinu Credi', kills: 0},
        {name:  'Dono da Bola',   kills: 0},
        {name:  'Mal',            kills: 0},
        {name:  'Chessus',        kills: 0}
    ]
};

let gameHistoric = [];

//load file:
//1-read line
//2-get game match data
//3-put game match data in a array
//4-every game match, reset data
rd.on('line', (data) => {

    //get all players kills xD
    for(player in game.players) {
        if(data.includes(`${game.players[player].name} killed`)) {
            game.players[player].kills ++;
            game.total_kills ++;
        } else if(data.includes(`<world> killed ${game.players[player].name}`)) {
            game.players[player].kills --;
            game.total_kills ++;
        }
    }

    // if game match ends
    if(data.includes('Exit: Fraglimit hit')) {
        //order by players kills 
        game.players.sort((a, b) => {
            if(a.kills < b.kills) return   1;
            if(a.kills > b.kills) return  -1;
            return 0;
        });
        //and put in a array
        gameHistoric.push(game);
        //reset game match
        game = {
            total_kills: 0,
            players: [
                {name:  'Oootsimo',       kills: 0},
                {name:  'Zeh',            kills: 0},
                {name:  'Isgalamido',     kills: 0},
                {name:  'Assasinu Credi', kills: 0},
                {name:  'Dono da Bola',   kills: 0},
                {name:  'Mal',            kills: 0},
                {name:  'Chessus',        kills: 0}
            ]
        };

    }

});

router.get('/', function(req, res) {
  res.render('index', {game: gameHistoric});
});

module.exports = router;